<!DOCTYPE html>
<html>
<head>
	<title>Login Siswa</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body class="login">
	<div class="pembungkus">
		<div class="card-content">
			<div class="card-title">
				<img src="logo-etikos.png" class="logo-login">
				<h2>LOGIN SISWA</h2>
			</div>
		</div>
		<form id="login" action="" method="post">
			<label class="label-judul">NIS</label>
			<input class="input-uname" type="number" name="nis" placeholder="Masukan NIS Anda" required>
			<label class="label-judul">Password</label>
			<span>
				<i id="pass-status" class="fa fa-eye" aria-hidden="true" onClick="viewPassword()"></i>
				<input class="input-pwd" type="password" id="password-field"  placeholder="Masukan Password Anda" name="password" value="secret" required>
			</span>
			<input class="input-submit" type="submit" name="login" value="Log in">
			<div class="link">
				<a class="lupapwd" href="#">Lupa Password?</a>			
			</div>
		</form>
	</div>
	<script type="text/javascript" src="js/script.js"></script>
</body>
</html>