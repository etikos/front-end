<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Etikos</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
    <div class="header">
        <div class="container position-fixed fixed-left">
          <img class="icon-sekola" src="img/sekola.png" alt="sekola">
          <div class="card-body">
            <div class="animasi-landing">
                <div class="row">
                    <h1 class="judul-landing">e- Voting Ketua Osis Smada</h1>
                </div>
                <div class="row mb-3">
                    <h7 class="subjudul-landing">Website Pemilihan Online Ketua Osis SMA 2 Paingan</h7>
                </div>
                <div class="row d-flex justify-content-center">
                    <button class="btn btn-light tombol-landing col-md-3 ml-2 mr-4 mb-2" onclick="location.href='loginAdmin.php';">Login Sebagai Admin</button>
                    <button class="btn btn-primary tombol-landing col-md-3 ml-2 mr-4 mb-2" onclick="location.href='loginSiswa.php';">Login Sebagai Siswa</button>
                </div>
            </div>
        </div>       
        <div class="fixed-bottom d-flex justify-content-end mb-3 mr-4">
            <a href="#" class="btn"><i class="fab fa-whatsapp fa-3x icon"></i></a>
            <a href="#" class="btn"><i class="fab fa-facebook fa-3x icon"></i></a>
            <a href="#" class="btn"><i class="fab fa-twitter fa-3x icon"></i></a>
            <a href="#" class="btn"><i class="fab fa-instagram fa-3x icon"></i></a>
        </div>
    </div>
</div>
</body>
</html>
