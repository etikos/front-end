<!DOCTYPE html>
<html>
<head>
	<title>Bantuan</title>
	<link rel="stylesheet" type="text/css" href="css/styleSiswaApp.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<style type="text/css">
.pembungkus-ubahpwd{
	position: absolute;
	border: 0.7px solid grey;
	border-radius: 6px;
	height: 350px;
	width: 650px;
	margin-top: 100px;
	left: 0;
	right: 0;
	margin-right: auto;
	margin-left: auto;
	padding-top: 20px;
	box-shadow: 0px 2px 12px 2px rgba(0,0,0,0.57);
}
.label-ubahpwd{
	display: block;
	font-size: 0.8em;
	color: grey; 
}
.tombol-ubahpwd{
	display: block;
	margin: 9px auto;
	font-size: 1em;
	padding: 5px 8px;
	width: 100px;
	background: #152349;
	outline: 0;
	border: none;
	border-radius: 5px;
	color: white;
}
.input-ubahpwd{
	background: #EEEEEE;
	color:black;
	width: 300px;
	padding: 8px 8px;
	margin-top: 5px;
	margin-bottom: 14px;
	box-sizing: border-box;
	border: 2px solid #EEEEEE;
	outline: none;
	border-radius: 5px;
	font-size: 0.8em;
	box-shadow: 4px 4px 4px -1px rgba(0,0,0,0.17);
}
.hidepwd{
	position: absolute; 
	margin-top: 15px;
	margin-left: 41%;
	width: 25px;
	background: #EEEEEE;
	padding-left: 10px;
}
.konten-pwd{
	height: 250px;
	width: 300px;
	margin-top: 30px;
	margin-right: auto;
	margin-left: auto;
}
.input-ubahpwd:hover{
	background: #F6F6F6;
}
.tombol-ubahpwd:hover{
	background: white;
	border: 1px solid;
	color: black;
}
.tombol-ubahpwd:active{
	transform: scale(0.9,0.9); 
}
</style>
<body>
	<div id="menu" class="pembungkus-navbar">
		<div class="pembungkus-menu">
			<div class="navbar-beranda">
				<img class="logo-navbar" src="img/logo-etikos.png" alt="logo" height="40px" width="40px">
				<img class="show-hide" src="img/burgericon.png" height="40px" width="40px" onclick="tampil()">
			</div>
			<div id="navbar-menu" class="navbar-menu">
				<div class="pembungkus-navbar-beranda">
					<span class="isi-menu">Beranda</span>
				</div>
				<div class="pembungkus-navbar-menu"  onclick="window.location='vote.php';">
					<span class="isi-menu">Vote</span>
				</div>
				<div class="pembungkus-navbar-menu" >
					<span class="isi-menu" onclick="window.location='tentang.php';">Tentang</span>
				</div>
				<div class="pembungkus-navbar-menu" >
					<span class="isi-menu" onclick="window.location='bantuan.php';">Bantuan</span>
				</div>		
			</div>
		</div>
		<div class="menu-profile">
			<img class="logo-profile" src="img/logo-etikos.png" alt="logo" height="40px" width="40px">
			<div class="label-profile">Tatag</div>
			<div class="pembungkus-menu-profile">	
				<div class="navbar-profile">
					<a class="isi-profile" href="ubahPwd.php">Ubah Password</a>
				</div>
				<div class="navbar-profile">
					<a class="isi-profile" href="">Logout</a>
				</div>	
			</div>
		</div>
	</div>

	<div class="pembungkus-ubahpwd">
		<h3 style="text-align: center;">Ubah Password</h3>
		<div class="konten-pwd">
		<form id="login" action="" method="post">
			<label class="label-ubahpwd">Password Lama</label>
			<span>
				<i class="fa fa-eye hidepwd" id="sembunyi1" onClick="viewChPassword('input1','sembunyi1')"></i>
				<input class="input-ubahpwd" id="input1" type="password" placeholder="Masukkan Password Lama Anda" required >
			</span>
			<label class="label-ubahpwd">Password Baru</label>
			<span>
				<i class="fa fa-eye hidepwd" id="sembunyi2" onClick="viewChPassword('input2','sembunyi2')"></i>
				<input class="input-ubahpwd" id="input2" type="password" placeholder="Masukkan Password Baru Anda" required>
			</span>
			<label class="label-ubahpwd">Konfirmasi Password</label>
			<span>
				<i class="fa fa-eye hidepwd" id="sembunyi3" onClick="viewChPassword('input3','sembunyi3')"></i>
				<input class="input-ubahpwd" id="input3"  type="password" placeholder="Ulangi Password Baru Anda" required>
			</span>
			<input class="tombol-ubahpwd" type="submit" name="login" value="Ubah">
		</form>
	</div>
</div>

<div class="footerSiswa">
	<span>Copyright&copy;2021 ETIKOS</span>
</div>
<script type="text/javascript" src="js/scriptSiswaApp.js"></script>
<script type="text/javascript" src="js/navbar.js"></script>
<script type="text/javascript">
	function viewChPassword(a,b){
		let pwdFiels = document.getElementById(a);
		let passStatus = document.getElementById(b);

		if (pwdFiels.type == "password") {
			pwdFiels.type = "text";
			passStatus.className='fa fa-eye-slash hidepwd';
		} else {
			pwdFiels.type = "password";
			passStatus.className='fa fa-eye hidepwd';

		}
	}
	
</script>
</body>
</html>